package pizzashop.food;

public interface OrderItem {
	String toString();
	double getPrice();
	double[] getPrices();
	int getSize();
	OrderItem clone();
}
