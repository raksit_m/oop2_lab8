package pizzashop.food;

public abstract class AbstractItem {
	
	protected int size;
	
	public AbstractItem(int size) {
		this.size = size;
	}
	
	public double getPrice(OrderItem item) {
		double price = 0;
		if ( size >= 0 && item.getSize() < item.getPrices().length ) {
			price += item.getPrices()[item.getSize()];
		}
		if(item instanceof Pizza) {
			for(Topping top: ((Pizza) item).getToppings()) {
				price += top.getPrice();
			}
		}
		return price;
	}
}
