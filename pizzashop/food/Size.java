package pizzashop.food;

public enum Size {
	None,
	Small,
	Medium,
	Large;
}