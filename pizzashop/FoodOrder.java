package pizzashop;
import java.util.*;

import pizzashop.food.*;

/** A customer order containing food items.
 *  The iterator provides an iterator over the FoodItems in the order.
 *  The Observable notifies observers whenever the order changes.
 *  The FoodOrder itself is attached as an Observer of FoodItems so
 *  it knows when a FoodItem changes.   
 */
public class FoodOrder implements Iterable {
	/** list of items in the order */
	private List<OrderItem> items;
	
	/** create a new, empty food order */
	public FoodOrder() {
		items = new ArrayList<OrderItem>();
	}
	/** add an item to the order.
	 *  @param item is the fooditem to add to order	
	 *  @return true if the item is successfully added
	 */
	public void add(OrderItem item) {
		items.add( item );
	}
	
	/** remove an item from the order */
	public void remove(OrderItem item) {
		// return result of ArrayList.remove()
		items.remove( item );
	}
	
	public void clear() {
		items.clear();
	}
	
	/** get the total price of the order
	 *  @return total price
	 */
	public double getPrice() {
		double price = 0;
		for( OrderItem item : items ) {
			price += item.getPrice();
		}
		return price;
	}
	
	/** return an iterator for the items in the order.
	 * This is easy since ArrayList is already Iterable.
	 */
	public Iterator<? extends OrderItem> iterator() {
		return items.iterator();
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		for( OrderItem item : items ) s.append(item.toString()+"\n");
		return s.toString();
	}
}